import React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';

export default class LoginPage extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('../resources/images/mockup-login-page.png')}
        style={styles.container} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
});
