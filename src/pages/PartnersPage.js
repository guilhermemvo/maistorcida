// https://medium.com/@oieduardorabelo/react-native-criando-grids-com-flatlist-b4eb64e7dcd5

import React from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

export default class PartnersPage extends React.Component {
  state = {
    data: [
      {
        id: "01",
        name: "ABC Rádio Táxi",
        imageURL: require("../resources/images/partners/abc-radio-taxi-cidade.png")
      }, {
        id: "02",
        name: "Academia Gruppe",
        imageURL: require("../resources/images/partners/academia-gruppe.png")
      }, {
        id: "03",
        name: "Ação e Movimento",
        imageURL: require("../resources/images/partners/acao-e-movimento.png")
      }, {
        id: "04",
        name: "Aiqfome",
        imageURL: require("../resources/images/partners/aiqfome.png")
      }, {
        id: "05",
        name: "Azarte Hookah",
        imageURL: require("../resources/images/partners/azarte-hookah.png")
      }, {
        id: "06",
        name: "Bar do Salim",
        imageURL: require("../resources/images/partners/bar-do-salim.png")
      }, {
        id: "07",
        name: "Barbearia John Kenidy",
        imageURL: require("../resources/images/partners/barbearia-john-kenidy.png")
      }, {
        id: "08",
        name: "Brazai Sushi",
        imageURL: require("../resources/images/partners/brazai-sushi.png")
      }, {
        id: "09",
        name: "Cathedral Fábrica Bar",
        imageURL: require("../resources/images/partners/cathedral-fabrica-bar.png")
      }, {
        id: "10",
        name: "Coronel Zacarias",
        imageURL: require("../resources/images/partners/coronel-zacarias.png")
      }, {
        id: "11",
        name: "Democrático Rock Bar",
        imageURL: require("../resources/images/partners/democratico-rock-bar.png")
      }, {
        id: "12",
        name: "Don Gabriel Barbearia",
        imageURL: require("../resources/images/partners/don-gabrielbarbearia.png")
      },
    ]
  }

  render() {
    const columns = 2;

    return (
      <ImageBackground
        source={require('../resources/images/bck-white.jpg')}
        style={styles.background}>

        <SafeAreaView>
          <FlatList
            data={createRows(this.state.data, columns)}
            dataArray={this.state.data}
            keyExtractor={item => item.id}
            numColumns={columns}
            style={styles.grid}
            renderItem={({ item }) => {
              if (item.empty) {
                return <View style={[styles.item, styles.itemEmpty]} />;
              }
              return <Image style={styles.item} source={item.imageURL} />;
            }}
          />
        </SafeAreaView>
      </ImageBackground>
    );
  }

  createRows(data, columns) {
    const rows = Math.floor(data.length / columns); // [A]
    let lastRowElements = data.length - rows * columns; // [B]
    while (lastRowElements !== columns) { // [C]
      data.push({ // [D]
        id: `empty-${lastRowElements}`,
        name: `empty-${lastRowElements}`,
        empty: true
      });
      lastRowElements += 1; // [E]
    }
    return data; // [F]
  }
}

function createRows(data, columns) {
  const rows = Math.floor(data.length / columns);
  let lastRowElements = data.length - rows * columns;

  while (lastRowElements !== columns) {
    data.push({
      id: `empty-${lastRowElements}`,
      name: `empty-${lastRowElements}`,
      empty: true
    });
    lastRowElements += 1;
  }

  return data;
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  grid: {
    padding: 10,
  },
  item: {
    // alignItems: "center",
    backgroundColor: '#fff',
    borderColor: "#62bb46",
    borderRadius: 15,
    borderWidth: 1,
    flex: 1,
    // flexBasis: 0,
    // flexGrow: 1,
    height: 100,
    margin: 10,
    resizeMode: "cover",
    width: 100,
  },
  itemEmpty: {
    backgroundColor: "transparent",
    borderWidth: 0,
  },
  text: {
    color: "#555"
  }
});
