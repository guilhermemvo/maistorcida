import React from 'react';
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native';

export default class ContactPage extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('../resources/images/bck-black.jpg')}
        style={styles.background}
      >
        <View style={styles.container}>
          <Text style={styles.title}>Contato</Text>

          <Image
            source={require('../resources/images/logo.png')}
            style={styles.logo} />

          <Text style={styles.text}>
            Endereço: Av. Mario Clapier Urbinatti, 370 - Apartam. 03
          </Text>

          <Text style={styles.maps}>
            MAPA
          </Text>

          <Text style={styles.text}>
            Email: contato@maistorcida.com.br
          </Text>

          <Text style={styles.text}>
            Telefone: (44) 99137-2825
          </Text>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },
  logo: {
    width: 250,
    height: 50,
  },
  maps: {
    color: 'white',
    fontSize: 100,
    padding: 20,
  },
  title: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
    paddingTop: 50,
  },
  text: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 15,
  },
});
