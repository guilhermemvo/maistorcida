import React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';

export default class CardPage extends React.Component {
  render() {
    return (
      <ImageBackground
        source={require('../resources/images/mockup-card-page.png')}
        style={styles.container} />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
});
