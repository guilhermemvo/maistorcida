import React, { Component } from 'react';
import { Text, Image } from 'react-native';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';

import CardPage from './pages/CardPage';
import ContactPage from './pages/ContactPage';
import LoginPage from './pages/LoginPage';
import PartnerPage from './pages/PartnerPage';
import PartnersPage from './pages/PartnersPage';

const DrawerStack = createDrawerNavigator({
  'Info Partners': {
     screen: PartnerPage,
     navigationOptions: {
       title: 'Parceiro',
     }
   }, 'Partners': {
     screen: PartnersPage,
     navigationOptions: {
       title: 'Parceiros',
     }
   }, 'Contato': {
     screen: ContactPage,
     navigationOptions: {
       title: 'Contato',
     }
   }, 'Card': {
    screen: CardPage,
    navigationOptions: {
      title: 'Carteirinha',
    }
  }, 'Login': {
    screen: LoginPage,
    navigationOptions: {
      title: 'Login',
    }
  },
}, {
  gesturesEnabled: false,
  // contentComponent: DrawerContainer
})

const drawerButton = (navigation) =>
  <Text
    style={{padding: 5, color: 'white'}}
    onPress={() => {
      navigation.navigate('DrawerOpen')
    }}>
    Menu
  </Text>

const logoRight = () =>
  <Image
    source={require('./resources/images/icon.png')}
    style={{width: 60, height: 35}} />

export const Drawer = createStackNavigator({
  DrawerStack: { screen: DrawerStack }
}, {
  headerMode: 'float',
  navigationOptions: ({navigation}) => ({
    headerStyle: {
      backgroundColor: '#8bc63e',
      borderBottomColor: '#62bb46',
      borderBottomWidth: 1,
    },
    headerTitleStyle: {
      color: 'white',
      fontSize: 30,
    },
    title: '',
    headerTintColor: 'white',
    gesturesEnabled: false,
    headerLeft: drawerButton(navigation),
    headerRight: logoRight()
  })
})
